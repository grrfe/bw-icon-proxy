FROM amazoncorretto:17-alpine3.19
ARG tag
COPY build/libs/bw-icon-proxy-$tag.jar /app.jar

CMD ["java", "-jar", "app.jar", "config.json"]
