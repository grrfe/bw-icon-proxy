plugins {
    application
    kotlin("jvm") version "1.9.24"
    id("net.nemerosa.versioning") version "3.1.0"
}

group = "fe.bw.iconproxy"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
    google()
}

kotlin {
    jvmToolchain(21)
}

dependencies {
    implementation("com.gitlab.grrfe.httpkt:core:13.0.0-alpha.62")
    implementation("com.gitlab.grrfe.httpkt:ext-gson:13.0.0-alpha.62")
    implementation("com.gitlab.grrfe.gson-ext:core:16.0.0-gson2-koin3")
    implementation("com.gitlab.grrfe.gson-ext:koin:16.0.0-gson2-koin3")
    implementation("com.gitlab.grrfe:kotlin-ext:0.0.59")

    implementation("org.slf4j:slf4j-simple:2.0.15")
    implementation("io.javalin:javalin:6.2.0")

    implementation("com.gitlab.grrfe.koin-app:core:0.0.9")
    implementation("com.gitlab.grrfe.koin-app:api:0.0.9")
    implementation("com.gitlab.grrfe.koin-app:exposed-core:0.0.9")
    implementation("com.gitlab.grrfe.koin-app:exposed-sqlite:0.0.9")


    implementation("io.insert-koin:koin-core:3.5.6")
    implementation("org.jetbrains.kotlinx:kotlinx-cli-jvm:0.3.5")
    implementation("com.gitlab.grrfe:kotlinx-cli-ext:0.0.2")
    implementation("com.google.code.gson:gson:2.11.0")
    implementation("io.github.microutils:kotlin-logging-jvm:3.0.5")
    implementation("org.jetbrains.exposed:exposed-core:0.52.0")
    implementation("org.jetbrains.exposed:exposed-dao:0.52.0")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.52.0")
    implementation("org.jetbrains.exposed:exposed-java-time:0.52.0")
    implementation("org.xerial:sqlite-jdbc:3.46.0.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.1")
    implementation("com.gitlab.grrfe:publicsuffixlistkt:0.0.1")
    implementation("redis.clients:jedis:5.2.0-beta2")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.1")
    implementation("com.squareup.okhttp3:okhttp:5.0.0-alpha.14")
    implementation("org.msgpack:msgpack-core:0.9.8")
    implementation("org.apache.commons:commons-compress:1.26.1")
    implementation("org.apache.sshd:sshd-core:2.13.1")
    implementation("org.apache.sshd:sshd-common:2.12.0")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("fe.bw.iconproxy.MainKt")
}

tasks.getByName<Jar>("jar") {
    manifest {
        attributes["Main-Class"] = application.mainClass.get()
    }

    excludes += setOf("META-INF/*.SF", "META-INF/*.DSA", "META-INF/*.RSA")

    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
