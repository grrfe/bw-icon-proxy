import java.util.Properties

rootProject.name = "bw-icon-proxy"

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://jitpack.io") }
    }
}

val dev = false
if (dev) {
    val properties = Properties().apply {
        file("local.properties").takeIf { it.exists() }?.bufferedReader()?.use { load(it) }
    }

    properties["koin-app.dir"]?.let { koinAppDir ->
        includeBuild(koinAppDir) {
            val substitutes = mapOf(
                "core" to "core",
                "api" to "api",
                "exposed-core" to "exposed:exposed-core",
                "exposed-sqlite" to "exposed:exposed-sqlite",
            )

            dependencySubstitution {
                for ((artifact, project) in substitutes) {
                    substitute(module("com.gitlab.grrfe.koin-app:$artifact")).using(project(":$project"))
                }
            }
        }
    }
}
