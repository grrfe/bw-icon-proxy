package fe.bw.iconproxy.module.perfectprivacy

import com.google.gson.Gson
import com.google.gson.JsonObject
import fe.gson.extension.json.element.`object`
import fe.gson.extension.json.`object`.asInt
import fe.gson.extension.json.`object`.map
import fe.httpkt.Request
import fe.httpkt.json.fromJson

class PerfectPrivacyConnector(private val gson: Gson) {
    private val request = Request()

    companion object {
        private const val TRAFFIC_ENDPOINT = "https://www.perfect-privacy.com/api/traffic.json"
    }

    fun fetch(): Set<String> {
        val trafficObj = request
            .get(TRAFFIC_ENDPOINT)
            .fromJson<JsonObject>(gson = gson)

        val servers = trafficObj.map { it.`object`() }.filter { (_, obj) ->
            obj.asInt("bandwidth_in") == -1 && obj.asInt("bandwidth_out") == -1
        }

        return servers.keys
    }
}
