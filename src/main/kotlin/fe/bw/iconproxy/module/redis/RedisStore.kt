package fe.bw.iconproxy.module.redis

import redis.clients.jedis.CommandArguments
import redis.clients.jedis.JedisPooled
import redis.clients.jedis.Protocol.Command
import redis.clients.jedis.params.SetParams

typealias SetParamFn = SetParams.() -> Unit

class RedisStore(
    host: String,
    port: Int,
    user: String? = null,
    password: String? = null,
) : JedisPooled(host, port, user, password) {

    fun tryPing() {
        executeCommand(CommandArguments(Command.PING))
    }

    fun set(key: ByteArray, value: ByteArray?, params: SetParamFn): String? {
        return set(key, value, SetParams().apply(params))
    }
}


