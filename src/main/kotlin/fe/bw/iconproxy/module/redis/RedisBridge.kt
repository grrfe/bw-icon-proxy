package fe.bw.iconproxy.module.redis

import fe.bw.iconproxy.config.RedisConfig
import fe.koin.helper.bridge.Bridge1

val RedisBridge: Bridge1<RedisConfig, RedisStore> = { config ->
    val password = if (config.password.isNullOrEmpty()) null else config.password
    RedisStore(config.host, config.port, null, password)
}
