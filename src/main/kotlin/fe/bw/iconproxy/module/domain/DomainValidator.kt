package fe.bw.iconproxy.module.domain

import kotlinx.coroutines.runBlocking
import mozilla.components.lib.publicsuffixlist.PublicSuffixList

class DomainValidator(private val publicSuffixList: PublicSuffixList) {
    fun isValid(domain: String): Boolean {
        if (domain.length < 3 || domain.length > 256 || !domain.contains(".")) return false
        if (domain.endsWith(".onion") || domain.endsWith(".i2p")) return false

        return runBlocking {
            val suffix = publicSuffixList.getPublicSuffix(domain).await() ?: return@runBlocking false
            publicSuffixList.isPublicSuffix(suffix).await()
        }
    }
}
