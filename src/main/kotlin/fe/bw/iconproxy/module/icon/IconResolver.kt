package fe.bw.iconproxy.module.icon

import fe.bw.iconproxy.module.ssh.SshProxyHandler
import fe.httpkt.Request
import fe.httpkt.ext.getGZIPOrDefaultStream
import fe.httpkt.ext.isHttpSuccess
import fe.httpkt.proxy.AuthProxy
import io.javalin.http.ContentType
import mu.KotlinLogging
import java.io.InputStream
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.SocketException

class IconResolver(private val handler: SshProxyHandler) {
    private val logger = KotlinLogging.logger(IconResolver::class.simpleName!!)

    private val cache = mutableMapOf<String, ProxiedGetRequest>()

    suspend fun requestIcon(domain: String): Result<DomainIcon> {
        val url = "https://icons.bitwarden.net/${domain}/icon.png"
        val localProxy = handler.nextProxy() ?: return Result.failure(Exception("No proxy available!"))
        val (tunnelId, localSocket) = localProxy

        val request = cache.getOrPut(tunnelId) { ProxiedGetRequest(localSocket) }

//        logger.info("Requesting $domain icon via ${request.data().proxy}")

        val con = try {
            request.get(url)
        } catch (e: SocketException) {
            logger.error("Failed to connect to $localProxy, retrying using next proxy..")
            handler.report(tunnelId)

            return requestIcon(domain)
        }

        if (!con.isHttpSuccess()) return Result.failure(RequestFailedException(con.responseCode))

        val icon = con.getGZIPOrDefaultStream()
        return Result.success(DomainIcon(con.contentType, con.contentLength.takeIf { it != -1 }, icon))
    }
}

data class ProxiedGetRequest(
    val localAddress: InetSocketAddress
) : Request(data = { this.proxy = AuthProxy(Proxy.Type.SOCKS, localAddress.hostName, localAddress.port) })

sealed class IconResolveException : Exception()
class InvalidDomainException : IconResolveException()
class RequestFailedException(val responseCode: Int) : IconResolveException()

class DomainIcon(val contentType: String, val contentLength: Int?, val icon: InputStream) {
    constructor(contentType: ContentType, icon: ByteArray) : this(
        contentType.toString(),
        icon.size,
        icon.inputStream()
    )
}
