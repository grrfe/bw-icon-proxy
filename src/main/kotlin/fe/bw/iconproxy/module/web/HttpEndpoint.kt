package fe.bw.iconproxy.module.web

import io.javalin.Javalin
import io.javalin.http.Handler
import io.javalin.http.HandlerType
import io.javalin.security.RouteRole

abstract class HttpEndpoint(
    vararg val paths: String,
    val roles: Array<out RouteRole> = emptyArray(),
    val method: HandlerType = HandlerType.GET
) : Handler

fun Javalin.addHttpEndpoint(vararg endpoints: HttpEndpoint): Javalin {
    for (endpoint in endpoints) {
        for (path in endpoint.paths) {
            addHttpHandler(endpoint.method, path, endpoint, *endpoint.roles)
        }
    }

    return this
}
