package fe.bw.iconproxy.module.web

import java.io.ByteArrayOutputStream
import java.io.InputStream

class CachedInputStream(
    private val stream: InputStream,
    size: Int?,
    val onClose: (ByteArray) -> Unit
) : InputStream() {

    private val buffer = ByteArrayOutputStream(size ?: 32)

    override fun read(): Int {
        val byte = stream.read()
        if (byte != -1) {
            buffer.write(byte)
        }

        return byte
    }

    override fun read(b: ByteArray): Int {
        val n = stream.read(b)
        if (n > 0) {
            buffer.write(b, 0, n)
        }

        return n
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        val n = stream.read(b, off, len)
        if (n > 0) {
            buffer.write(b, off, n)
        }

        return n
    }

    override fun readAllBytes(): ByteArray {
        val bytes = stream.readAllBytes()
        buffer.write(bytes, 0, bytes.size)

        return bytes
    }

    override fun readNBytes(len: Int): ByteArray {
        val bytes = stream.readNBytes(len)
        buffer.write(bytes, 0, len)

        return bytes
    }

    override fun readNBytes(b: ByteArray, off: Int, len: Int): Int {
        val n = stream.readNBytes(b, off, len)
        if (n != -1) {
            buffer.write(b, off, n)
        }

        return n
    }

    override fun close() {
        stream.close()
        onClose(buffer.toByteArray())
    }
}
