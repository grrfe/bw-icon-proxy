package fe.bw.iconproxy.module.web


import io.javalin.http.Context
import io.javalin.http.HandlerType
import io.javalin.security.RouteRole
import io.javalin.validation.Validator


abstract class HttpParam1Endpoint<T : Any>(
    private val route: Route,
    roles: Array<out RouteRole> = emptyArray(),
    method: HandlerType = HandlerType.GET
) : HttpEndpoint(route.path, roles = roles, method = method) {

    override fun handle(ctx: Context) {
        val (validator1) = route.scope.createValidators(ctx)

        @Suppress("UNCHECKED_CAST")
        handle(ctx, validator1 as Validator<T>)
    }

    abstract fun handle(ctx: Context, validator1: Validator<T>)
}
