@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
@file:OptIn(ExperimentalEncodingApi::class)

package fe.bw.iconproxy.module.web

import fe.bw.iconproxy.module.domain.DomainValidator
import fe.bw.iconproxy.module.icon.DomainIcon
import fe.bw.iconproxy.module.icon.IconResolver
import fe.bw.iconproxy.module.redis.RedisStore
import fe.bw.iconproxy.util.MagicFileBytes
import io.javalin.http.ContentType
import io.javalin.http.Context
import io.javalin.http.HttpStatus
import io.javalin.validation.Validator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.future.future
import mu.KotlinLogging
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi
import kotlin.time.Duration.Companion.days

@ExperimentalEncodingApi
object IconApiEndpoint : HttpParam1Endpoint<String>(
    route { "/icons/${path<String>("domain")}/icon.png" }
), KoinComponent {
    private val coroutineScope = CoroutineScope(Dispatchers.Default)
    private val iconResolver by inject<IconResolver>()

    private val domainValidator by inject<DomainValidator>()
    private val redisStore by inject<RedisStore>()

    private val fallbackIcon = DomainIcon(
        ContentType.IMAGE_PNG,
        IconApiEndpoint::class.java.getResourceAsStream("/fallback-icon.png")!!.use { it.readBytes() }
    )

    private val duration = 7.days
    private val logger = KotlinLogging.logger(IconApiEndpoint::class.simpleName!!)

    override fun handle(ctx: Context, domainParam: Validator<String>) {
        val domain = domainParam.get()
        val encodedDomain = Base64.encodeToByteArray(domain.encodeToByteArray())

        if (!domainValidator.isValid(domain)) {
            ctx.status(HttpStatus.BAD_REQUEST)
            return
        }

        val timeLeft = redisStore.pexpireTime(encodedDomain)
        if (timeLeft > 0) {
            val icon = redisStore.get(encodedDomain)
            val magicFileBytes = MagicFileBytes.findOrNull(icon)

            ctx.contentType(magicFileBytes?.type ?: ContentType.OCTET_STREAM).result(icon)
            return
        }

        ctx.future {
            coroutineScope.future(Dispatchers.IO) {
                val result = iconResolver.requestIcon(domain)
                writeResponse(ctx, result, encodedDomain)
            }
        }
    }

    private fun writeResponse(ctx: Context, result: Result<DomainIcon>, cacheKey: ByteArray): Context {
        val domainIcon = result.getOrDefault(fallbackIcon)
        val stream = CachedInputStream(domainIcon.icon, domainIcon.contentLength) { writtenBytes ->
            redisStore.set(cacheKey, writtenBytes) { ex(duration.inWholeSeconds) }
        }

        return ctx.contentType(domainIcon.contentType).result(stream)
    }
}


