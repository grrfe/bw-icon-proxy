package fe.bw.iconproxy.module.web

import fe.koin.helper.bridge.Bridge0
import io.javalin.Javalin
import mu.KotlinLogging
import java.time.LocalDateTime
import kotlin.io.encoding.ExperimentalEncodingApi

@OptIn(ExperimentalEncodingApi::class)
val JavalinBridge: Bridge0<Javalin> = {
    val logger = KotlinLogging.logger("Javalin")
    val app = Javalin.create { cfg ->
        cfg.showJavalinBanner = false
        cfg.requestLogger.http { ctx, executionTimeMs ->
            logger.info(
                "%s: %s %s -> %s (took %.2fms)".format(
                    LocalDateTime.now().toString(),
                    ctx.method(),
                    ctx.matchedPath(),
                    ctx.status(),
                    executionTimeMs
                )
            )
        }
    }

    app.addHttpEndpoint(IconApiEndpoint)

    for (httpHandler in app.unsafeConfig().pvt.internalRouter.allHttpHandlers()) {
        logger.info(
            "[Route] %-6s %-30s %s".format(
                httpHandler.endpoint.method,
                httpHandler.endpoint.path,
                httpHandler.endpoint.handler::class.simpleName
            )
        )
    }

    app
}
