package fe.bw.iconproxy.module.web

import io.javalin.http.Context
import io.javalin.http.pathParamAsClass
import io.javalin.http.queryParamAsClass
import io.javalin.validation.Validator

data class Route(val path: String, val scope: RouteScope)

inline fun route(buildRoute: RouteScope.() -> String): Route {
    val scope = RouteScope(1)
    val path = buildRoute(scope)

    return Route(path, scope)
}

@DslMarker
annotation class PathParamRouteDsl

typealias CreateValidator<T> = (Context) -> Validator<T>

@PathParamRouteDsl
class RouteScope(val maxSize: Int) {
    val pathParams = mutableSetOf<String>()
    val queryParams = mutableSetOf<String>()

    val validators = mutableListOf<CreateValidator<*>>()

    val currentSize: Int
        get() = pathParams.size + queryParams.size

    fun createValidators(ctx: Context): List<Validator<*>> {
        return validators.map { it(ctx) }
    }

    inline fun <reified T : Any> path(name: String): String {
        return addParameter<T>(pathParams, name) { ctx -> ctx.pathParamAsClass<T>(name) }
    }

    inline fun <reified T : Any> query(name: String): String {
        return addParameter<T>(queryParams, name) { ctx -> ctx.queryParamAsClass<T>(name) }
    }

    inline fun <reified T : Any> addParameter(set: MutableSet<String>, name: String, noinline fn: CreateValidator<T>): String {
        require(name !in set)
        require(currentSize < maxSize)

        validators.add(fn)
        set.add(name)
        return "{$name}"
    }
}
