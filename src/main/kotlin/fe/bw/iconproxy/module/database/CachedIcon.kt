package fe.bw.iconproxy.module.database

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object CachedIcons : IntIdTable("cached_icons") {
    val domain = varchar("domain", 256)
    val icon = blob("icon")
    val timestamp = long("timestamp")
}

class CachedIcon(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CachedIcon>(CachedIcons)

    var domain by CachedIcons.domain
    var icon by CachedIcons.icon
    var timestamp by CachedIcons.timestamp
}
