package fe.bw.iconproxy.module.ssh

import fe.bw.iconproxy.config.TunnelsConfig
import fe.bw.iconproxy.module.perfectprivacy.PerfectPrivacyConnector
import fe.bw.iconproxy.util.KeyHelper
import fe.koin.helper.bridge.Bridge2
import org.apache.sshd.client.SshClient
import org.apache.sshd.client.auth.password.UserAuthPasswordFactory
import org.apache.sshd.client.config.hosts.HostConfigEntryResolver
import org.apache.sshd.common.keyprovider.KeyIdentityProvider
import org.apache.sshd.common.session.SessionHeartbeatController
import org.apache.sshd.server.forward.AcceptAllForwardingFilter
import kotlin.time.Duration.Companion.minutes
import kotlin.time.toJavaDuration


val SshProxyHandlerBridge: Bridge2<TunnelsConfig, PerfectPrivacyConnector, SshProxyHandler> = { config, connector ->
    val client = setUpDefaultClient()
    val downServers = connector.fetch()

    val randomTunnels = config.tunnels
        .filter { it.host !in downServers }
        .random(config.count)

    val servers = randomTunnels.map { tunnel ->
        val publicKey = KeyHelper.parseKnownHostKey(tunnel.keyAlgo, tunnel.pubKey)
        SshServer(tunnel.host, tunnel.port, tunnel.username, tunnel.password, publicKey)
    }

    client.serverKeyVerifier = ConfigServerKeyVerifier(servers)
    client.setSessionHeartbeat(SessionHeartbeatController.HeartbeatType.IGNORE, 1.minutes.toJavaDuration())

    SshProxyHandler(client, servers)
}

private fun setUpDefaultClient(): SshClient {
    return SshClient.setUpDefaultClient().apply {
        forwardingFilter = AcceptAllForwardingFilter.INSTANCE
        keyIdentityProvider = KeyIdentityProvider.EMPTY_KEYS_PROVIDER
        userAuthFactories = listOf(UserAuthPasswordFactory.INSTANCE)
        hostConfigEntryResolver = HostConfigEntryResolver.EMPTY
    }
}

private fun <T> List<T>.random(count: Int): MutableSet<T> {
    val set = toMutableSet()

    val items = mutableSetOf<T>()
    repeat(count) {
        val item = set.random()
        set.remove(item)

        items.add(item)
    }

    return items
}
