package fe.bw.iconproxy.module.ssh

import fe.kotlin.iterable.linkedList
import kotlinx.coroutines.*
import mu.KotlinLogging
import org.apache.sshd.client.SshClient
import org.apache.sshd.client.session.forward.DynamicPortForwardingTracker
import org.apache.sshd.common.util.net.SshdSocketAddress
import java.io.IOException
import java.net.ServerSocket
import java.security.PublicKey
import java.util.UUID
import kotlinx.coroutines.channels.*
import java.net.InetSocketAddress

typealias TunnelConnection = Pair<SshTunnel, DynamicPortForwardingTracker>
typealias ProxyTicket = Pair<String, InetSocketAddress>

class SshProxyHandler(private val sshClient: SshClient, private val servers: List<SshServer>) {
    private val logger = KotlinLogging.logger(SshProxyHandler::class.java.simpleName)
    private var proxyQueue: ProxyQueue? = null

    fun start() {
        sshClient.start()

        val tunnels = linkedList<Pair<SshTunnel, DynamicPortForwardingTracker>>()
        for (server in servers) {
            val result = connect(server) ?: continue
            tunnels.add(result)
        }

        proxyQueue = ProxyQueue(tunnels)
    }

    fun report(tunnelId: String) {
        proxyQueue?.report(tunnelId)
    }

    suspend fun nextProxy(): ProxyTicket? {
        return proxyQueue?.next()
    }

    private fun connect(server: SshServer): Pair<SshTunnel, DynamicPortForwardingTracker>? {
        val id = UUID.randomUUID().toString()
        val tunnel = SshTunnel(id, sshClient, server)

        try {
            val tracker = tunnel.connect()
            return tunnel to tracker
        } catch (e: IOException) {
            logger.error("Connection failed!", e)
        }

        return null
    }
}

class SshServer(
    val host: String,
    val port: Int,
    val username: String,
    val password: String,
    val publicKey: PublicKey,
) {
}

class SshTunnel(
    val tunnelId: String,
    val client: SshClient,
    val server: SshServer,
    val bindTo: String = "localhost",
    val connectTimeout: Long = 10_000L,
    val authTimeout: Long = 10_000L
) {
    private var localProxy: SshdSocketAddress? = null
    private val logger = KotlinLogging.logger("SshTunnel @ ${server.host}")

//    private val eventChannel = Channel<TunnelEvent>(Channel.UNLIMITED)

    @Throws(IOException::class)
    fun connect(): DynamicPortForwardingTracker {
        logger.info("Connecting..")

        val connection = client.connect(server.username, server.host, server.port).verify(connectTimeout)
        val session = connection.session

        session.addPasswordIdentity(server.password)
        session.auth().verify(authTimeout)

        logger.info("Connected!")

        logger.info("Searching for free port..")
//        val localPort = withContext(Dispatchers.IO) {
        val localPort = ServerSocket(0).use { it.localPort }
//        }

        logger.info("Found port $localPort!")

        val address = SshdSocketAddress(bindTo, localPort)

        logger.info("Starting proxy on $address..")
        val tracker = session.createDynamicPortForwardingTracker(address)

        logger.info("Proxy is ready!")
        return tracker
    }
}




