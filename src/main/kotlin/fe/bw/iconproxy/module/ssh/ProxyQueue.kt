package fe.bw.iconproxy.module.ssh

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import java.util.*

class ProxyQueue(private val queue: Queue<TunnelConnection>) {
    private val logger = KotlinLogging.logger(ProxyQueue::class.java.simpleName)
    private val mutex = Mutex()

    private val tunnels = queue.associate { (tunnel, _) -> tunnel.tunnelId to tunnel }
    private val cooldownPool = mutableMapOf<String, Long>()

    suspend fun next(): ProxyTicket {
        val nextProxy: TunnelConnection? = mutex.withLock { queue.poll() }
        if (nextProxy == null) {
            error("Out of proxies!")
        }

        val (tunnel, tracker) = nextProxy
        if (!tracker.session.isOpen) {
            logger.error("Tunnel $tunnel is closed, adding to cooldown pool")
            report(tunnel.tunnelId)
            // TODO: Check PP serverstatus

            return next()
        }

        if (!tracker.isOpen) {
            // Lost binding?
            logger.error("Tunnel $tunnel has no active port forwarding")
            return next()
        }

        val added = mutex.withLock { queue.offer(nextProxy) }
        if (!added) logger.error("Failed to enqueue $tunnel")

        return tunnel.tunnelId to tracker.localAddress.toInetSocketAddress()
    }

    fun report(tunnelId: String) {
        cooldownPool[tunnelId] = System.currentTimeMillis()
    }
}
