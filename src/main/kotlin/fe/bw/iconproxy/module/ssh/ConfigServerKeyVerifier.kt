package fe.bw.iconproxy.module.ssh

import org.apache.sshd.client.keyverifier.ServerKeyVerifier
import org.apache.sshd.client.session.ClientSession
import org.apache.sshd.common.config.keys.KeyUtils
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.security.PublicKey

class ConfigServerKeyVerifier(servers: List<SshServer>) : ServerKeyVerifier {
    private val map = servers.associateBy { tunnel -> tunnel.host }

    override fun verifyServerKey(
        clientSession: ClientSession,
        remoteAddress: SocketAddress,
        serverKey: PublicKey,
    ): Boolean {
        if (remoteAddress !is InetSocketAddress) return false

        val expectedServer = map[remoteAddress.hostString] ?: return false
        return KeyUtils.compareKeys(expectedServer.publicKey, serverKey)
    }
}
