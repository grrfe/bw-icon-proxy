package fe.bw.iconproxy.config

import fe.koin.exposed.config.LocalDatabaseConfig
import fe.koin.exposed.wrapper.DefaultDatabase
import fe.koin.helper.config.KoinConfig
import fe.koin.helper.config.KoinRootConfig

data class Config(
    val tunnelConfig: TunnelsConfig,
    val webConfig: WebConfig,
    val redisConfig: RedisConfig,
    val sqliteConfig: LocalDatabaseConfig<DefaultDatabase>,
) : KoinRootConfig<Config>

data class RedisConfig(
    val host: String,
    val port: Int,
    val password: String? = null
) : KoinConfig<RedisConfig>

data class TunnelsConfig(
    val count: Int,
    val tunnels: List<TunnelConfig>
) : KoinConfig<TunnelsConfig>

data class TunnelConfig(
    val host: String,
    val port: Int,
    val username: String,
    val password: String,
    val pubKey: String,
    val keyAlgo: String
) : KoinConfig<TunnelConfig>

data class CachingConfig(
    val enabled: Boolean = true,
    val directory: String,
) : KoinConfig<CachingConfig>

data class WebConfig(
    val host: String? = "0.0.0.0",
    val port: Int
) : KoinConfig<WebConfig>
