package fe.bw.iconproxy.util

import org.apache.sshd.common.config.keys.KeyUtils
import org.apache.sshd.common.config.keys.PublicKeyEntry
import java.security.PublicKey

object KeyHelper {
    private fun decodeKeyData(keyType: String, data: String): ByteArray {
        val resolver = PublicKeyEntry.resolveKeyDataEntryResolver(keyType)
        return resolver.decodeEntryKeyData(data)
    }

    private fun resolveKey(keyType: String, decodedData: ByteArray): PublicKey {
        val decoder = KeyUtils.getPublicKeyEntryDecoder(keyType)
        return decoder.resolve(null, keyType, decodedData, emptyMap())
    }

    fun parseKnownHostKey(keyType: String, data: String): PublicKey {
        return resolveKey(keyType, decodeKeyData(keyType, data))
    }
}
