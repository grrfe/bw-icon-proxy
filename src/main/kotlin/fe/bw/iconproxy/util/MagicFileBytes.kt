package fe.bw.iconproxy.util

import io.javalin.http.ContentType
import org.apache.hadoop.io.FastByteComparisons

class MagicFileBytes(val type: String, private vararg val magicBytes: Byte) {
    constructor(type: ContentType, vararg magicBytes: Byte) : this(type.mimeType, *magicBytes)

    fun check(bytes: ByteArray): Boolean {
        if (bytes.size < magicBytes.size) return false

        return FastByteComparisons.compareTo(magicBytes, 0, magicBytes.size, bytes, 0, magicBytes.size) == 0
    }

    companion object {
        val PNG = MagicFileBytes(ContentType.IMAGE_PNG, -119, 80, 78, 71)
        val X_ICON = MagicFileBytes("image/x-icon", 0, 0, 1, 0)
        val WEBP = MagicFileBytes(ContentType.IMAGE_WEBP, 82, 73, 70, 70)
        val JPEG = MagicFileBytes(ContentType.IMAGE_JPEG, -1, -40, -1)
        val GIF = MagicFileBytes(ContentType.IMAGE_GIF, 71, 73, 70, 56)
        val BMP = MagicFileBytes(ContentType.IMAGE_BMP, 66, 77)

        private val lookup = arrayOf(
            PNG, X_ICON, WEBP, JPEG, GIF, BMP
        )

        fun findOrNull(bytes: ByteArray): MagicFileBytes? {
            return lookup.find { it.check(bytes) }
        }
    }
}

