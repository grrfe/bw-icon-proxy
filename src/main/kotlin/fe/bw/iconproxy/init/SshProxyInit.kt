package fe.bw.iconproxy.init

import fe.bw.iconproxy.module.ssh.SshProxyHandler
import fe.koin.helper.init.KoinInitializer

class SshProxyInit(private val handler: SshProxyHandler) : KoinInitializer {
    override fun init() {
        handler.start()
    }
}
