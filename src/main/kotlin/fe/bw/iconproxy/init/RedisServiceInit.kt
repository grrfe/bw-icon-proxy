package fe.bw.iconproxy.init

import fe.bw.iconproxy.module.redis.RedisStore
import fe.koin.helper.init.KoinInitializer

class RedisServiceInit(private val redisStore: RedisStore) : KoinInitializer {
    override fun init() {
        tryOrFail("Redis is not reachable") {
            redisStore.tryPing()
        }
    }
}
