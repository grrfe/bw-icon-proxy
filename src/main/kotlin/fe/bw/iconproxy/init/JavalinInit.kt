package fe.bw.iconproxy.init

import fe.bw.iconproxy.config.WebConfig
import fe.koin.helper.init.KoinInitializer
import io.javalin.Javalin


class JavalinInit(private val webConfig: WebConfig, private val javalin: Javalin) : KoinInitializer {
    override fun init() {
        javalin.start(webConfig.host, webConfig.port)
    }
}
