package fe.bw.iconproxy.init

import fe.koin.helper.init.KoinInitializer
import kotlinx.coroutines.runBlocking
import mozilla.components.lib.publicsuffixlist.PublicSuffixList

class PublicSuffixListInit(private val publicSuffixList: PublicSuffixList) : KoinInitializer {
    override fun init() = runBlocking {
        publicSuffixList.prefetch().await()
    }
}
