package fe.bw.iconproxy

import fe.bw.iconproxy.config.Config
import fe.bw.iconproxy.init.JavalinInit
import fe.bw.iconproxy.init.PublicSuffixListInit
import fe.bw.iconproxy.init.RedisServiceInit
import fe.bw.iconproxy.init.SshProxyInit
import fe.bw.iconproxy.module.database.CachedIcons
import fe.bw.iconproxy.module.domain.DomainValidator
import fe.bw.iconproxy.module.icon.IconResolver
import fe.bw.iconproxy.module.perfectprivacy.PerfectPrivacyConnector
import fe.bw.iconproxy.module.redis.RedisBridge
import fe.bw.iconproxy.module.web.JavalinBridge
import fe.bw.iconproxy.module.ssh.SshProxyHandlerBridge
import fe.gson.extension.io.fromJson
import fe.gson.globalGsonModule
import fe.koin.exposed.database.SQLite
import fe.koin.exposed.init.DefaultDatabaseInit
import fe.koin.exposed.module.database.localDatabase
import fe.koin.exposed.wrapper.DefaultDatabase
import fe.koin.helper.app.KoinApp
import fe.koin.helper.config.ConfigScope
import fe.koin.helper.ext.createInstance
import fe.koin.helper.init.InitializerScope
import fe.kotlinx.cli.util.ExistingFileArgType
import fe.publicsuffixlist.Context
import kotlinx.cli.ArgParser
import mozilla.components.lib.publicsuffixlist.PublicSuffixList
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.ModuleDeclaration

object BwIconProxyApp : KoinApp<Config>(modules = listOf(globalGsonModule)) {
    override val config: ConfigScope<Config> = {
        config(it::tunnelConfig)
        config(it::redisConfig)
        config(it::webConfig)
        config(it::sqliteConfig)
    }

    override val appModule: ModuleDeclaration = {
        single { DefaultDatabase.localDatabase(get(), SQLite) }
        single { PublicSuffixList(Context) }
        singleOf(::PerfectPrivacyConnector)
        singleOf(::IconResolver)
        singleOf(::DomainValidator)
        singleOf(RedisBridge)
        singleOf(SshProxyHandlerBridge)
        singleOf(JavalinBridge)
    }

    override val initializer: InitializerScope = {
        init { DefaultDatabaseInit(get<DefaultDatabase>(), CachedIcons) }
        initOf(::PublicSuffixListInit)
        initOf(::RedisServiceInit)
        initOf(::SshProxyInit)
        initOf(::JavalinInit)
    }
}

fun main(args: Array<String>) {
    val parser = ArgParser("bw-icon-proxy")
    val configPath by parser.argument(ExistingFileArgType)
    parser.parse(args)

    val config = configPath.fromJson<Config>()
    val app = BwIconProxyApp.createInstance(config)

    app.start()
}


